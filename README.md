# DWM

You also need to clone the scripts repo for everything to work well, especially the bar.

Dependencies:

*  dmenu
*  rofi
*  xorg-xsetroot
*  dunst
*  acpilight or xorg-xbacklight
*  pulsemixer


`sudo pacman -S dmenu xorg-xsetroot dunst acpilight pulsemixer`

NOTE: You need to add the user to the video group to contol the bringhtness with the fn keys.
    `sudo usermod -a -G video <username>`

**dwm-black**

![alt text](https://gitlab.com/S_ThirtyFive/dwm/-/raw/master/dwm_black.png)

![alt text](https://gitlab.com/S_ThirtyFive/dwm/-/blob/77d4cef38edee212e113f5a00eceead6b5637ea5/dwm/dwm-color.png)

I use a custom GTK theme [Black-Is-The-New-Black](https://gitlab.com/S_ThirtyFive/black-GTK)

**Custom Shortcuts:**

- `Mod+Ctrl+f` Firefox
- `Mod+Ctrl+p` Firefox Private Window
- `Mod+Ctrl+r` Ranger file Manager
- `Mod+Ctrl+c` Cmus Music Player


For the rest, checkout the congig.def.h file

**Status Bar**

I use xsetroot to set the status bar, if you plan on cloning this repo, please clone my [bar scripts](https://gitlab.com/S_ThirtyFive/my_scripts/-/tree/master/bar) aswell.

`git clone https://gitlab.com/S_ThirtyFive/my_scripts.git`

Copy all the contents in the repo to /usr/bin or any of your $PATH and make it excecutable. 

The repo also contains dmenu scripts for cmus control and connecting bluetooth devices.


**Dmenu**

I use my custom build of Dmenu, please find it here or feel free to use your own. 

> I have'nt themed dmenu in the config.def.h because I use my custom build, feel free to change it or cuild your own build of dmenu.

**Terminal**

I use Alacritty as my terminal, change it in the config.def.h

> Apps like ranger and cmus are also launched from the terminal, if you are planning on using another terminal, it is imortant that you change it.






