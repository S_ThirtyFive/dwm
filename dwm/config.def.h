/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int gappx     = 5;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char statussep         = ';';      /* separator between status bars */
static const char *fonts[]          = { "SourceCodePro-Light:size=10" };

static const char norm_fg[]     = "#ddbfae";
static const char norm_bg[]     = "#09030d";
static const char norm_border[] = "#9a8579";

static const char sel_fg[]      = "#ddbfae";
static const char sel_bg[]      = "#6E5050";
static const char sel_border[]  = "#ff0000";

static const char urg_fg[]      = "#ddbfae";
static const char urg_bg[]      = "#682126";
static const char urg_border[]  = "#682126";



static const char *colors[][3]      = {

    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win

        };

/* tagging */
static const char *tags[] = { "", "", "", "", "", "", "", "", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
#include "fibonacci.c"
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
 	{ "[@]",      spiral },
 	{ "[\\]",     dwindle },
        { "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]      = { "dmenu_run", NULL };
static const char *termcmd[]       = { "alacritty", NULL };
static const char *vollower[]      = { "pulsemixer", "--change-volume", "-5",  NULL };
static const char *volraise[]	   = { "pulsemixer", "--change-volume", "+5",  NULL  };
static const char *volmute[]       = { "pulsemixer", "--toggle-mute",  NULL };
static const char *backlightup[]   = { "/usr/bin/xbacklight", "-inc", "1",     NULL };
static const char *backlightdown[] = { "/usr/bin/xbacklight", "-dec", "1",     NULL };
static const char *clipboard[]	   = { "clipmenu", "-i", "-b", "-l", "3", NULL };
static const char *bluetoothcmd[]     = { "bluetooth_menu", NULL };
static const char *firefoxcmd[]    = { "firefox", NULL };
static const char *privatecmd[]    = { "firefox", "--private-window", NULL };
static const char *clifmcmd[]      = { "alacritty", "-e", "ranger", NULL };
static const char *cmuscmd[]       = { "alacritty", "-e", "cmus", NULL };
static const char *telegramcmd[]   = { "telegram-desktop", NULL };
static const char *powercmd[]      = { "power_menu", NULL };

#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */

    /*Launch Stuff*/
        { MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
        { MODKEY|ControlMask,           XK_f,      spawn,          {.v = firefoxcmd } },
	{ MODKEY|ControlMask,           XK_p,      spawn,          {.v = privatecmd } },
	{ MODKEY|ControlMask,           XK_r,      spawn,          {.v = clifmcmd } },
	{ MODKEY|ControlMask,           XK_c,      spawn,          {.v = cmuscmd } },
	{ MODKEY|ControlMask,           XK_t,      spawn,          {.v = telegramcmd } },
    	{ MODKEY,                       XK_c,      spawn,          {.v = clipboard } },
    	{ MODKEY|ShiftMask,             XK_b,      spawn,          {.v = bluetoothcmd } },
        { MODKEY|ShiftMask,             XK_e,      spawn,          {.v = powercmd } },
   

        /*Vol-Backlight*/
        { 0,                            XK_F8,     spawn,          {.v = volraise } },
    	{ 0,                            XK_F7,     spawn,          {.v = vollower } },
    	{ 0,                            XK_F6,     spawn,          {.v = volmute } },
    	{ 0,                            XK_F2,     spawn,          {.v = backlightdown } },
    	{ 0,                            XK_F3,     spawn,          {.v = backlightup } },
    /*bar*/
        { MODKEY,                       XK_b,      togglebar,      {0} },
    /**/
        { MODKEY,                       XK_x,      togglesticky,   {0} },
    /*Window Focus*/
        { MODKEY,                       XK_Up,     focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_Down,   focusstack,     {.i = -1 } },
    /**/
        { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
    /*Master Size*/
        { MODKEY|ControlMask,           XK_Down,   setmfact,       {.f = -0.01} },
	{ MODKEY|ControlMask,           XK_Up,     setmfact,       {.f = +0.01} },
    /*Slave Size*/	
        { MODKEY|ShiftMask,             XK_h,      setcfact,       {.f = +0.01} },
	{ MODKEY|ShiftMask,             XK_l,      setcfact,       {.f = -0.01} },
	{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
    /*Move Stack*/
        { MODKEY|ShiftMask,             XK_Up,     movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Down,   movestack,      {.i = -1 } },

        { MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY|ControlMask,           XK_Return, view,           {0} },

        { MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
    /*Change Layout*/
        { MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_u,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_o,      setlayout,      {.v = &layouts[4]} },
        { MODKEY,                       XK_r,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY|ShiftMask,             XK_r,      setlayout,      {.v = &layouts[6]} },

        { MODKEY,                       XK_space,  setlayout,      {0} },
        { MODKEY,                       XK_Tab,    cyclelayout,    {.i = +1 } },
        { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },

        { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },

        { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },

        { MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
        /*Switch Tags*/
        TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_x,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
