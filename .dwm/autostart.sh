#!/bin/sh

~/.dwm/bar & sleep 1 &&


#Clipboard Manager with dmenu
#clipmenud &

#dunst &

#This enables tap to click, see how to configure it to your system in the readme file.
xinput set-prop 10 312 1

#nitrogen --restore &

#Connman notification manager
#connman-notify &

# System monitor
##s35monitor &

#auto wallpaper changer
#~/.config/wallpaper/wallpaper &

#xautolock -time 10 -locker ~/.config/lock/lock.sh &

